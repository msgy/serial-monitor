# -*- coding: UTF-8 -*-
# Copyright 2020 YangZhongxi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
from asyncio import transports
from datetime import time

import serial
import asyncio
import serial_asyncio
from PyQt5 import QtCore

from PyQt5.QtCore import QObject

import async_serial
from AsyncDataReceiver import AsyncDataReceiver


class SerialMonitor(QObject):

    def __init__(self, filename, listener, physical_port_name, physical_port_rate, virtual_port_name, virtual_port_rate):
        """
        :param filename: 存储文件名
        :param listener: 数据监听对象，基于QT的信号/槽机制
        :param physical_port_name: 物理串口
        :param physical_port_rate: 物理串口波特率
        :param virtual_port_name: 虚拟串口
        :param virtual_port_rate: 虚拟串口波特率
        """
        super().__init__()
        self.physical_port = physical_port_name
        self.virtual_port = virtual_port_name
        self.receiver = AsyncDataReceiver(filename, listener)
        self.physical_serial = async_serial.AsyncSerialPort(physical_port_name, physical_port_rate, self.physical_receiver)
        self.virtual_serial = async_serial.AsyncSerialPort(virtual_port_name, virtual_port_rate, self.virtual_receiver)

    def Start(self):
        """
        启动监听
        :return: True-成功，False-失败
        """
        self.physical_serial.open()
        self.virtual_serial.open()
        self.receiver.Start()

    def Stop(self):
        self.physical_serial.close()
        self.virtual_serial.close()
        self.receiver.Stop()

    def physical_receiver(self, data: bytes):
        self.virtual_serial.write(data)  # 收到物理串口数据，转到虚拟串口
        self.receiver.AddData(self.physical_port, data)

    def virtual_receiver(self, data: bytes):
        self.physical_serial.write(data)  # 收到虚拟串口数据，转到物理串口
        self.receiver.AddData(self.virtual_port, data)
