# serial-monitor

## 监听原理
**图1. 串口监听原理**  
![image](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9ub3RlLnlvdWRhby5jb20veXdzL3B1YmxpYy9yZXNvdXJjZS8yMjJjMzQyNTZhNGMzNzFjNDllNTJjNTRiY2I4MTMzNi82MTU2NkI4MjA3NzE0NjkxQjU1RjhERjk4NUU3NTkyMQ?x-oss-process=image/format,png)

&emsp;如图1所示，COM3为物理串口，正常使用的情况下，上位机软件会直接打开COM3进行通信。为实现监听功能，增加虚拟串口对及监听软件，**上位机不直接连接物理串口，而是连接虚拟串口的一端，使用监听软件打开物理串口及虚拟串口的另一端**，实现数据的透传及监听功能。

- **虚拟串口**: 虚拟串口是一对互通的虚拟设备，当向COM1写入数据时，COM2收到数据；向COM2写入数据时，COM1收到数据。
- **监听软件**: 实现串口监听的软件实现，主要业务是打开COM1和COM3。收到COM3数据时，转发给COM1；收到COM1数据时,转发给COM3；同时将转发的数据记录下来。

## 虚拟串口

&emsp;虚拟串口软件比较常用的主要有[Virtual Serial Port Driver](https://www.eltima.com/products/vspdxp/)以及[com0com](http://com0com.sourceforge.net/)，前者收费，后者GPL协议开源。这里使用com0com，安装好软件后创建虚拟串口对`COM11<->COM12`，在设备管理器中可以看到虚拟设备  
**图2. 虚拟串口设备**  
![虚拟串口](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9ub3RlLnlvdWRhby5jb20veXdzL3B1YmxpYy9yZXNvdXJjZS8yMjJjMzQyNTZhNGMzNzFjNDllNTJjNTRiY2I4MTMzNi82OEE4QzFCMEFBQzg0ODIyOUJCQTFEMkFBNjBCMDBCQQ?x-oss-process=image/format,png)
