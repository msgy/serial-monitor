# coding:utf-8

from PyQt5.QtWidgets import QDialog
from PyQt5 import QtCore
import ui_setting_dialog
import serial
import serial.tools.list_ports


class SettingDialog(QDialog, ui_setting_dialog.Ui_ListenSettingDialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.SettingBtn.clicked.connect(self.settingBtnClick)
        self.CancelBtn.clicked.connect(self.cancelBtnClick)

        # self.setWindowFlags(QtCore.Qt.CustomizeWindowHint | QtCore.Qt.WindowCloseButtonHint)
        self.setFixedSize(QtCore.QSize(468, 135))
        self.PhysicalPortName.setEditable(True)
        self.PhysicalSerialRate.setEditable(True)
        self.VirtualPortName1.setEditable(True)
        self.VirutalSerailRate1.setEditable(True)
        rates = ["9600", "19200", "38400", "115200"]
        # 枚举串口, 用此方法，在windows不能枚举vspd虚拟串口???
        ports = serial.tools.list_ports.comports()
        for i in ports:
            port_list = list(i)
            self.PhysicalPortName.addItem(port_list[0])
            self.VirtualPortName1.addItem(port_list[0])

        self.PhysicalSerialRate.addItems(rates)
        self.VirutalSerailRate1.addItems(rates)
        self.IsSetting = False

    def settingBtnClick(self):
        self.IsSetting = True
        self.close()

    def cancelBtnClick(self):
        self.IsSetting = False
        self.close()

    def getPhysicalPortName(self):
        return self.PhysicalPortName.currentText()

    def getPhysicalPortRate(self):
        return self.PhysicalSerialRate.currentText()

    def getVirtualPortName(self):
        return self.VirtualPortName1.currentText()

    def getVirtualPortRate(self):
        return self.VirutalSerailRate1.currentText()
